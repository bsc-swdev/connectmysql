
# Task 4: Database Technique JDBC Seminar

The focal point of the assignment is to connect a Java program via IntelliJ to the MySQL server's sample classicmodels database.

A requirement was also that the use of Statement's executeQuery() and executeUpdate() be demonstrated.


## Secondary goal

In addition to the above requirements, the opportunity was also taken to explore the inner workings of Java Annotations.

The aim is to auto generate sql select statement from a DTO, and populating DTO objects from cursor (ResultSet)

into a DTO, readily available for use.
