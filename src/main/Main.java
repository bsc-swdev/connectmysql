package main;

import java.util.List;
import main.SQLContract.*;
import main.dto.CustomerDTO;
import main.dto.OrderDTO;
import main.enums.OrderStatus;
import static main.CRUDUtils.*;

public class Main {

    private static List<CustomerDTO> customerDTOList;
    private static List<OrderDTO>  orderDTOList;

    public static void main(String[] args) {
        CRUDUtils crudUtils = new CRUDUtils();

        // Get a list of all customers, whose city is London.
        String selectStmt = "SELECT " + Customer.ID + " FROM " + Customer.TABLE +
                " WHERE city = 'london'";
        customerDTOList = crudUtils.getTableAsDTO(selectStmt, CustomerDTO.class);
        assert customerDTOList != null;
        Integer[] idArr = customerDTOList.stream()
                .map(CustomerDTO::getCustomerNumber)
                .toArray(Integer[]::new);

        // Get dtos of all orders matching the customer ids from above query
        String whereClause = getWhereInClause(Customer.ID, (Object[]) idArr);
        selectStmt = "SELECT " + Order.allColumns() + " FROM " + Order.TABLE + whereClause;
        orderDTOList = crudUtils.getTableAsDTO(selectStmt, OrderDTO.class);

        printText(customerDTOList.toString());

        // UPDATE

        OrderDTO dto = null;

        if (idArr.length > 0) {
            Integer customerId = idArr[0];
            printText(String.valueOf(customerId));
            // Modify index 0 of the List, orderNumber='10129',  status='Cancelled'
            dto = orderDTOList.stream()
                    .filter(orderDTO -> (int) orderDTO.getCustomerNumber() == (int) customerId )
                    .findAny()
                    .orElse(null);
        }

        // Modify the status of the order to 'On Hold'
        if (dto != null) {
            dto.status = OrderStatus.ON_HOLD.getStatus();
            int rowsAffected = crudUtils.update(dto);
            printText("main: rows = " + rowsAffected);
        }

        // Clean up resources
        crudUtils.onDestroy();
    }


    public static void printText(CharSequence ... string) {
        for ( CharSequence s : string) {
            System.out.println(s);
        }
    }
}
