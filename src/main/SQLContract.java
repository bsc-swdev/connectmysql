package main;

import main.dto.CustomerDTO;
import main.dto.DTOColumn;
import main.dto.OrderDTO;
import org.jetbrains.annotations.VisibleForTesting;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static main.CRUDUtils.removeBrackets;

/**
 * Utils for managing global SQL values.
 */
public class SQLContract {

    /**
     * Customer Table
     */
    public static class Customer implements ContractSchema<CustomerDTO> {

        public static Class<CustomerDTO> dtoClass = CustomerDTO.class;

        /**
         * Table name
         */
        public static String TABLE = "customers";

        /**
         * Customer number column
         */
        public static String ID = "customerNumber";

        @Override
        public Class<CustomerDTO> getDTOClass() {
            return dtoClass;
        }

        @Override
        public String getTable() {
            return TABLE;
        }

        @Override
        public String getID() {
            return ID;
        }
    }


    /**
     * Order Table
     */
    public static class Order implements ContractSchema<OrderDTO> {

        public static Class<OrderDTO> dtoClass = OrderDTO.class;

        /**
         * Table name
         */
        public static String TABLE = "orders";

        /**
         * Order number column
         */
        public static String ID = "orderNumber";

        /**
         * Return a list of all column names associated with dto.
         */
        public static String allColumns() {
            // "orderNumber, orderDate, requiredDate, shippedDate, status, comments, customerNumber"
            return getAnnotationFields(OrderDTO.class);
        }

        @Override
        public Class<OrderDTO> getDTOClass() {
            return dtoClass;
        }

        @Override
        public String getTable() {
            return TABLE;
        }

        @Override
        public String getID() {
            return ID;
        }
    }

    public static <T> String[] getAnnotationFieldsArr(@DTORecord Class<T> cls) {
       List<String> fieldList = new ArrayList<>();
        for (Field field : cls.getDeclaredFields()) {
            // Determine whether there is an annotated name present, or whether to use field name.
            String annoTag = field.getAnnotation(DTOColumn.class).column();
            String columnName = annoTag.equals(DTOColumn.NONE) ? field.getName() : annoTag;
            if (!columnName.equals("")) {
                fieldList.add(columnName);
            }
        }
        return fieldList.toArray(new String[0]);
    }

    /**
     * Return annotated fields as String with comma, ",", as delimiter.
     * E.g. name1, name2, name3 ... nameN
     */
    @VisibleForTesting
    public static <T> String getAnnotationFields(@DTORecord Class<T> cls) {
        String[] fieldsArr = getAnnotationFieldsArr(cls);
        // Remove brackets from the toString output and return
        return removeBrackets(Arrays.toString(fieldsArr));
    }
}
