package main.dto;

import main.DTORecord;

@DTORecord
public class CustomerDTO {

    @DTOColumn
    public Integer customerNumber;

    public CustomerDTO() { }

    public Integer getCustomerNumber() {
        return customerNumber;
    }
}
