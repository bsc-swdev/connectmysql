package main.dto;

import main.DTORecord;

@DTORecord
public class OrderDTO {

    @DTOColumn
    public Integer orderNumber;

    @DTOColumn
    public String orderDate;

    @DTOColumn
    public String requiredDate;

    @DTOColumn
    public String shippedDate;

    @DTOColumn
    public String status;

    @DTOColumn
    public String comments;

    @DTOColumn
    public Integer customerNumber;

    public OrderDTO() {
    }

    public OrderDTO(Integer orderNumber, String orderDate, String requiredDate, String shippedDate,
                    String status, String comments, Integer customerNumber) {
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
        this.requiredDate = requiredDate;
        this.shippedDate = shippedDate;
        this.status = status;
        this.comments = comments;
        this.customerNumber = customerNumber;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public Integer getCustomerNumber() {
        return customerNumber;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "orderNumber='" + orderNumber + '\'' +
                ", orderDate='" + orderDate + '\'' +
                ", requiredDate='" + requiredDate + '\'' +
                ", shippedDate='" + shippedDate + '\'' +
                ", status='" + status + '\'' +
                ", comments='" + comments + '\'' +
                ", customerNumber='" + customerNumber + '\'' +
                '}';
    }

}
