package main;

import main.dto.CustomerDTO;
import main.dto.DTOColumn;
import main.dto.OrderDTO;
import main.enums.DDLMode;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static main.SQLContract.getAnnotationFieldsArr;

public class StmtUtils {

    private StmtUtils() {
        throw new AssertionError("Private constructor, cannot be instantiated.");
    }

    public static <T> String parseUpdateClause(ContractSchema<T> contract) {
        String placeHolder = " = ?, ";
        StringBuilder builder = new StringBuilder("UPDATE " + contract.getTable() + " SET ");
        for (String column : getAnnotationFieldsArr(contract.getDTOClass())) {
            builder.append(column).append(placeHolder);
        }
        return builder.substring(0, builder.length() - 2) + " WHERE " + contract.getID() + " = ?";
    }

    /**
     * Generate a PreparedStatement with placeholders created to match DTO signature.
     */
    public static <T> PreparedStatement getPreparedStatement(@DTORecord Class<T> cls, DDLMode mode) {
        try {
            Connection conn = DatabaseConnection.getInstance();
            ContractSchema<?> contract;
            String clsName = cls.getCanonicalName();
            if (clsName.equals(CustomerDTO.class.getCanonicalName())) {
                contract = new SQLContract.Customer();
            } else if (clsName.equals(OrderDTO.class.getCanonicalName())) {
                contract = new SQLContract.Order();
            } else {
                throw new IllegalStateException("DTOClass not implemented");
            }
            String clause = "";
            switch (mode) {
                case UPDATE -> {
                    clause = StmtUtils.parseUpdateClause(contract);
                }
                case INSERT, DELETE -> {
                    // TODO
                }
            }
            return conn.prepareStatement(clause);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Helper method for processing Annotations linked to a particular DTOClass.
     */
    public static <T> List<T> processAnnotations(ResultSet cursor, @DTORecord Class<T> cls) {
        List<T> dtoList = new ArrayList<>();
        try {
            while (cursor.next()) {
                // printText("processAnnotations: while entered");
                T dto = cls.getConstructor().newInstance();

                for (Field field : cls.getDeclaredFields()) {
                    // Determine the qualifying column name
                    String annoTag = field.getAnnotation(DTOColumn.class).column();
                    String columnName = annoTag.equals(DTOColumn.NONE) ? field.getName() : annoTag;
                    // Get corresponding value from ResultSet's cursor
                    Class<?> type = field.getType();
                    // printText("processAnnotations: type = " + type.toString());
                    switch (type.getCanonicalName()) {
                        case "java.lang.String":
                            // printText("processAnnotations: String.");
                            String valueStr = cursor.getString(columnName);
                            field.set(dto, valueStr);
                            break;
                        case "java.lang.Integer":
                            // printText("processAnnotations: Integer.");
                            Integer valueInt = cursor.getInt(columnName);
                            field.set(dto, valueInt);
                            break;
                        default:
                            throw new IllegalStateException("Type not currently handled!");
                    }

                }
                dtoList.add(dto);
            }
            if (!cursor.isClosed()) {
                cursor.close();
            }
        } catch (SQLException | NoSuchMethodException |
                IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return dtoList;
    }


    /**
     * Helper method for processing Annotations linked to a particular DTOClass.
     */
    public static <T> PreparedStatement processAnnotationsForInsert(@DTORecord Object dto, PreparedStatement pStmt) {
        //List<String> list = new ArrayList<>();
        try {
            Class<?> cls = dto.getClass();

            int pos = 1;
            for (Field field : cls.getDeclaredFields()) {
                // Determine the qualifying column name
                String annoTag = field.getAnnotation(DTOColumn.class).column();
                String columnName = annoTag.equals(DTOColumn.NONE) ? field.getName() : annoTag;
                // Get corresponding value from ResultSet's cursor
                Class<?> type = field.getType();
                // printText("processAnnotations: type = " + type.toString());
                switch (type.getCanonicalName()) {
                    case "java.lang.String":
                        // printText("processAnnotations: String.");
                        pStmt.setString(pos++, String.valueOf(field.get(dto)));
                        // list.add(String.valueOf(field.get(dto)));
                        break;
                    case "java.lang.Integer":
                        // printText("processAnnotations: Integer.");
                        pStmt.setInt(pos++, Integer.parseInt(String.valueOf(field.get(dto))));
                        //list.add(String.valueOf(field.getInt(dto)));
                        break;
                    default:
                        throw new IllegalStateException("Type not currently handled!");
                }

            }
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
        // list.toArray(new String[0])
        return pStmt;
    }

    public static <T> PreparedStatement processAnnotationsForUpdate(@DTORecord Object dto, PreparedStatement pStmt) {
        try {
            PreparedStatement pStmtReturn = processAnnotationsForInsert(dto, pStmt);
            // Add primary key to the where placeholder.
            KeyInfo info = getPrimaryKeyInfo(dto);
            if (info == null)
                throw new NullPointerException("PrimaryKey could not be extracted.");
            pStmtReturn.setString(info.parameterIndex, info.primaryKey);
            return pStmtReturn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class KeyInfo {
        public String primaryKey;
        public int parameterIndex;

        public KeyInfo(String primaryKey, int parameterIndex) {
            this.primaryKey = primaryKey;
            this.parameterIndex = parameterIndex;
        }
    }

    public static KeyInfo getPrimaryKeyInfo(Object dto) {
        try {
            Field[] fieldArr = dto.getClass().getDeclaredFields();
            return new KeyInfo(String.valueOf(fieldArr[0].get(dto)), fieldArr.length + 1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}
