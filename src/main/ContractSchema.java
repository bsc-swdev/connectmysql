package main;

public interface ContractSchema <T> {
    Class<T> getDTOClass();
    String getTable();
    String getID();
}
