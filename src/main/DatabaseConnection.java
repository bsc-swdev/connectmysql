package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static main.Main.printText;

/**
 * Singleton connection given the project is connected to only one database.
 */
public final class DatabaseConnection {

    private static Connection instance = null;

    private DatabaseConnection() { }

    /**
     * Return a connection object, to server, if process is success, else null.
     */
    public static Connection getInstance() {
        if (instance == null) {
            synchronized (DatabaseConnection.class) {
                try (FileInputStream fis = new FileInputStream("src/resources/db.properties");) {
                    // Load the input params to getConnection: url, user, password
                    Properties p = new Properties();
                    p.load(fis);
                    // create a connection to the database with db params
                    instance = DriverManager.getConnection(
                            p.getProperty("url"), p.getProperty("user"), p.getProperty("password")
                    );
                } catch(SQLException | IOException e) {
                    printText(e.getMessage());
                }
            }
        }
        return instance;
    }

}
