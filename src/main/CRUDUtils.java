package main;

import main.enums.DDLMode;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static main.Main.printText;
import static main.StmtUtils.*;

public class CRUDUtils {

    private List<Statement> statementList;

    public CRUDUtils() {
        statementList =new ArrayList<>();
    }

    /**
     * Return a result-set cursor matching the select query.
     */
    public ResultSet select(String query) throws SQLException {
        Connection conn = DatabaseConnection.getInstance();
        Statement stmt = conn.createStatement();
        statementList.add(stmt);
        return stmt.executeQuery(query);
    }

    /**
     * Update all the attributes (Columns) of the give DTO (Table) in the Server.
     */
    public int update(@DTORecord Object dto) {
        PreparedStatement updatePStmt = getPreparedStatement(dto.getClass(), DDLMode.UPDATE);
        statementList.add(updatePStmt);
        assert updatePStmt != null;
        int rowsAffected = 0;
        try {
            PreparedStatement loadedPS = processAnnotationsForUpdate(dto, updatePStmt);
            printText(String.valueOf(loadedPS));
            assert loadedPS != null;
            rowsAffected = loadedPS.executeUpdate();
            if (!loadedPS.isClosed())
                loadedPS.close();
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return rowsAffected;
    }

    /**
     * A Generalised method for transferring cursor data to a Plain Old Java Object.
     * @param selectStmt SQL select query
     * @param dtoClass DTO object with which an attempt to transfer cursor data to, is made
     * @return a List containing the appropriate DTO objects
     */
    public <T> List<T> getTableAsDTO(String selectStmt, @DTORecord Class<T> dtoClass) {
        try {
            ResultSet cursor = select(selectStmt);
            // Offload the cursor to an ArrayList
            return processAnnotations(cursor, dtoClass);
        } catch (SQLException e) {
            printText(e.getMessage());
        }
        return null;
    }

    /**
     * Closes Statements and singleton Connection, if active.
     */
    public void onDestroy() {
        try {
            for (Statement s : statementList)
                if (!s.isClosed()) s.close();
            Connection conn = DatabaseConnection.getInstance();
            if (!conn.isClosed()) conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Concatenate the values into the format of a where in clause: 'WHERE IN (var1, var2, varN)'.
     * Overload for types inheriting from Object.
     * @param column is the column name (aka attribute)
     */
    public static String getWhereInClause(String column, Object ... values) {
        return whereInHelper(column, removeBrackets(Arrays.toString(values)));
    }

    /**
     * Concatenate the values into the format of a where in clause: 'WHERE IN (var1, var2, varN)'.
     * Overload for primitive type: int.
     * @param column is the column name (aka attribute)
     */
    public static String getWhereInClause(String column, int ... values) {
        return whereInHelper(column, removeBrackets(Arrays.toString(values)));
    }

    /**
     * Concatenate the values into the format of a where in clause: 'WHERE IN (var1, var2, varN)'.
     * Overload for primitive type: double.
     * @param column is the column name (aka attribute)
     */
    public static String getWhereInClause(String column, double ... values) {
        return whereInHelper(column, removeBrackets(Arrays.toString(values)));
    }

    public static String whereInHelper(String column, String arrToString) {
        // toString produces: [1.6, 4.34, 6.6, 8.89] for double primitive type
        // [1, 4, 6, 8] for int primitive type, [[e1, e2, eN]] for List and Object[T] arrays
        return " WHERE " + column + " IN (" +  arrToString + ") ";
    }

    public static String removeBrackets(String arrToString) {
        return arrToString.replaceAll("\\[", "").replaceAll("\\]", "");
    }


}
