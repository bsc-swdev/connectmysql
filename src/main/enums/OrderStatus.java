package main.enums;

public enum OrderStatus {
    SHIPPED("Shipped"),
    CANCELLED("Cancelled"),
    RESOLVED("Resolved"),
    ON_HOLD("On Hold");

    private final String status;

    public String getStatus() {
        return status;
    }

    OrderStatus(String status) {
        this.status = status;
    }
}
