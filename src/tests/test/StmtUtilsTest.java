package test;

import main.SQLContract;
import main.StmtUtils;
import main.dto.OrderDTO;
import org.junit.Test;

import static org.junit.Assert.*;

public class StmtUtilsTest {

    @Test
    public void parseUpdateClause() {
        String expected = "UPDATE " + SQLContract.Order.TABLE  + " SET " +
                "orderNumber = ?, orderDate = ?, requiredDate = ?, shippedDate = ?, " +
                "status = ?, comments = ?, customerNumber = ?" +
                " WHERE orderNumber = ?";
        assertEquals(expected, StmtUtils.parseUpdateClause(new SQLContract.Order()));
    }

    /**
     * Returns element 0 of field values, which represents the PK.
     */
    @Test
    public void getPrimaryKey() {
        OrderDTO dto = new OrderDTO(10129, "2003-06-12", "2003-06-18",
                "2003-06-14", "Shipped", "null", 324);
        StmtUtils.KeyInfo info = StmtUtils.getPrimaryKeyInfo(dto);
        assert info != null;
        assertEquals(10129, Integer.parseInt(info.primaryKey));
        // Returns correct placeholder for WHERE condition column name
        assertEquals(8, info.parameterIndex);
    }
}