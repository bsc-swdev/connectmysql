package test;

import main.ContractSchema;
import main.SQLContract;
import main.dto.CustomerDTO;
import main.dto.OrderDTO;
import org.junit.Test;

import static org.junit.Assert.*;

public class SQLContractTest {

    @Test
    public void customerDto_static() {
        // dtoClass field is overridden.
        assertEquals(CustomerDTO.class, SQLContract.Customer.dtoClass);

        // table field is overridden
        String expected = "customers";
        assertEquals(expected, SQLContract.Customer.TABLE);
    }

    @Test
    public void customerDto_instance() {
        ContractSchema<CustomerDTO> customer = new SQLContract.Customer();
        // dtoClass field is overridden.
        assertEquals(CustomerDTO.class, customer.getDTOClass());

        // table field is overridden
        String expected = "customers";
        assertEquals(expected, customer.getTable());
    }

    @Test
    public void getAnnotationFields() {
        String expected = "orderNumber, orderDate, requiredDate, shippedDate, status, " +
                "comments, customerNumber";
        assertEquals(expected, SQLContract.getAnnotationFields(OrderDTO.class));
    }
}